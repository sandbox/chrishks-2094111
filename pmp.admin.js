/**
 * Implement page manager permissions - hide links which the user doesn't have
 * access to. The 'menu tabs' as implemented by hook_menu is taken care of by
 * Drupal, this is a quick fix to tidy the UI.od links.which the user may not
 * be able to access.
 */
(function($) {
  Drupal.behaviors.pmp = {
    attach: function(context, settings) {
      $.each(Drupal.settings.pmp, function(i) {
        // Build up an array of selectors to hide based on the permissions we have
        // been given to exclude.
        var selectors = [];

        $.each(Drupal.settings.pmp[i], function(j, value) {
          var permission = value.split(' ');

          // 'administer page manager' is provided by page_manager and is needed
          // to add/delete/enable/disable pages.
          if (value == 'administer page manager') {
            selectors.push(
              'a[href*="admin/structure/pages/add"]',
              'a[href*="admin/structure/pages/nojs/enable"]',
              'a[href*="admin/structure/pages/nojs/disable"]'
            );
          }

          // Page manager listing table.
          if (permission[0] == 'edit') {
            $.each(['page-', 'search-'], function(k, term) {
              if (permission[1].search(term) === 0) {
                permission[1] = permission[1].replace(term, '');
              }
            });
            selectors.push('tr.page-task-' + permission[1]);
          }

          // Access variant tabs for all pages.
          if (permission[0] == 'access' && permission[1] == 'variant') {
            selectors.push('a[href$="' + permission[2] + '"]');
          }

          // Perform actions on a variant within a specific page.
          if (permission[0] == 'access' && permission[2] == 'actions') {
            selectors.push(
              '.actions',
              'a[href$="actions/add"]',
              'a[href$="actions/delete"]',
              'td.page-summary-operation a[href$="actions/enable"],',
              'td.page-summary-operation a[href$="actions/disable"]'
            );
          }

          // Edit settings on a variant within a specific page.
          if (permission[0] == 'access' && permission[2] == 'settings') {
            selectors.push(
              '.operations-settings',
              '.page-manager-group-title:contains("Settings")',
              'a[href$="actions/disable"]',
              'a[href$="settings/basic"]',
              'a[href$="settings/access"]',
              'a[href$="settings/menu"]'
            );
          }

        });

        // Filter out duplicates and convert to comma-separated string.
        selectors = selectors.filter(function(e, i, array) {
          return array.lastIndexOf(e) === i;
        }).join(', ');

        // Hide the selected elements.
        $(selectors, context).hide().parent('li').hide();
      });
    }
  };
})(jQuery);
